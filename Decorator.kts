interface Batido {
    fun getSabor()
}

class concretaBatido : Batido {
    override fun getSabor() {
        println("Leche !")
    }
}
open class DecoradorBatido(protected var batido: Batido) : Batido {
    override fun getSabor() {
        this.batido.getSabor()
    }
}

class batidoFrutilla(m:Batido) : DecoradorBatido(m){

    override public fun getSabor(){
        super.getSabor ();
        this.addSabor();
        println(" Es un batido de frutillas !");
    }
    public fun addSabor(){
        println(" Añadiendo frutillas al batido !");
    }
}

public class batidoPlatano(m:Batido) : DecoradorBatido(m){

    override public fun getSabor(){
        super.getSabor ();
        this.addSabor();
        println(" Es un batido de plátano !");
    }
    public fun addSabor(){
        println(" Añadiendo plátano al batido !");
    }
}

fun main( ) {
    val batidoFrutilla = batidoFrutilla(concretaBatido())
    batidoFrutilla.getSabor()
    val batidoPlatano = batidoPlatano(concretaBatido())
    batidoPlatano.getSabor()

}

main()